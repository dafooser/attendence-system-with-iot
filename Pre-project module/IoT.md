![image](https://gitlab.com/Master-Ajeet/attendence-system-with-iot/-/raw/master/Pre-project%20module/Extra/IoT.png)

#  What is the internet of things?
The IoT brings the power of the internet, data processing and analytics to the real world of physical objects. For consumers, this means interacting with the global information network without the intermediary of a keyboard and screen; many of their everyday objects and appliances can take instructions from that network with minimal human intervention.
In enterprise settings, IoT can bring the same efficiencies to physical manufacturing and distribution that the internet has long delivered for knowledge work. Millions if not billions of embedded internet-enabled sensors worldwide are providing an incredibly rich set of data that companies can use to gather data about their safety of their operations, track assets and reduce manual processes. Researchers can also use the IoT to gather data about people's preferences and behavior, though that can have serious implications for privacy and security.

#  How does the IoT work?

The basic elements of the IoT are devices that gather data. Broadly speaking, they are internet-connected devices, so they each have an IP address. They range in complexity from autonomous vehicles that haul products around factory floors to simple sensors that monitor the temperature in buildings. They also include personal devices like fitness trackers that monitor the number of steps individuals take each day. To make that data useful it needs to be collected, processed, filtered and analyzed, each of which can be handled in a variety of ways.

Collecting the data is done by transmitting it from the devices to a gathering point. Moving the data can be done wirelessly using a range of technologies or on wired networks. The data can be sent over the internet to a data center or a cloud that has storage and compute power or the transfer can be staged, with intermediary devices aggregating the data before sending it along.

Processing the data can take place in data centers or cloud, but sometimes that’s not an option. In the case of critical devices such as shutoffs in industrial settings, the delay of sending data from the device to a remote data center is too great. The round-trip time for sending data, processing it, analyzing it and returning instructions (close that valve before the pipes burst) can take too long. In such cases edge-computing can come into play, where a smart edge device can aggregate data, analyze it and fashion responses if necessary, all within relatively close physical distance, thereby reducing delay. Edge devices also have upstream connectivity for sending data to be further processed and stored.
![image](https://gitlab.com/Master-Ajeet/attendence-system-with-iot/-/raw/master/Pre-project%20module/Extra/nw_how_iot_works_diagram-100840757-large.jpg)

#  IoT communication standards and protocols
When IoT gadgets talk to other devices, they can use a wide variety of communications standards and protocols, many tailored to devices with limited processing capabilities or not much electrical power. Some of these you've definitely heard of — some devices use Wi-Fi or Bluetooth, for instance — but many more are specialized for the world of IoT. ZigBee, for instance, is a wireless protocol for low-power, short-distance communication, while message queuing telemetry transport (MQTT) is a publish/subscribe messaging protocol for devices connected by unreliable or delay-prone networks. (See Network World’s glossary of IoT standards and protocols.)


%%======================================Your Region================================================================%%